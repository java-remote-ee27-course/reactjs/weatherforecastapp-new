# Weather Forecast App

A modern ReactJS Weather app, which I converted from an old ReactJS class-based app to the new function-based app.

## Features

- search any location, get 7 days real-time weather forecast
- fetch weather data from https://geocoding-api.open-meteo.com and https://api.open-meteo.com
- last location is saved in the local storage, and fetched after page reload
- uses useState, useEffect and two custom hooks:
  - useLocalStorageState (to save and fetch data from browser's local storage)
  - useWeatherState (to fetch locations data from a web API and weather data from another API in web)

## Limits

- in case two (or more) places have one name, the search results are limited to the first place found
- errors fetching data etc. are only logged to console right now

## Old versions

(Current code also contains my codes from the old, class-based versions:

- App-v1.js (initial, w/o lifecycle methods, w/o saving data to local storage)
- App-v2.js - the class-based version w lifecycle methods, and saving data to local storage)

- My class-based version are also available here: https://gitlab.com/java-remote-ee27-course/reactjs/weatherforecastapp

## Pictures

![weather1](./public/weather1.png)

## Created by

Katlin

## Acknowledgement

- Styles based on J. Schmedtmann's https://www.udemy.com/course/the-ultimate-react-course/learn/lecture/37350912
- getWeatherIcon and formatDay functions taken from: J. Schmedtmann's course https://www.udemy.com/course/the-ultimate-react-course/learn/lecture/37350912
- My class-based versions are based on J.Schmedtmann's course mentioned above.
