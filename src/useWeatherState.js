import { useEffect, useState } from "react";

export function useWeatherState(location) {
  const [isLoading, setIsLoading] = useState(false);
  const [displayLocation, setDisplayLocation] = useState({});
  const [weather, setWeather] = useState({});

  //const [location, setLocation] = useLocalStorageState("location", "Napoli");

  useEffect(() => {
    async function fetchWeather() {
      if (location?.length < 2) return setWeather({});

      try {
        setIsLoading(true);

        // 1) Getting location (geocoding)
        const geoRes = await fetch(
          `https://geocoding-api.open-meteo.com/v1/search?name=${location}`
        );

        const geoData = await geoRes.json();

        if (!geoData.results) throw new Error("Location not found");

        const { latitude, longitude, timezone, name, country_code } =
          geoData.results.at(0);

        setDisplayLocation({ city: name, flag: country_code.toLowerCase() });

        // 2) Getting actual weather
        const weatherRes = await fetch(
          `https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&timezone=${timezone}&daily=weathercode,temperature_2m_max,temperature_2m_min`
        );

        //if the weather data not available for this location
        if (weatherRes === undefined || weatherRes?.status === 400) {
          setWeather({});
          throw new Error("Weather data not available!");
        }

        const weatherData = await weatherRes.json();

        setWeather(weatherData.daily);

        console.log("Weather: ", weatherData.daily);
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoading(false);
      }
    }
    fetchWeather();
  }, [location]);

  return { isLoading, weather, displayLocation };
}
