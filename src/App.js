import { useLocalStorageState } from "./useLocalStorageState";
import { useWeatherState } from "./useWeatherState";

function getWeatherIcon(wmoCode) {
  const icons = new Map([
    [[0], "☀️"],
    [[1], "🌤️"],
    [[2], "⛅"],
    [[3], "☁️"],
    [[45, 48], "🌫️"],
    [[51, 56, 61, 66, 80], "🌦️"],
    [[53, 55, 63, 65, 57, 67, 81, 82], "🌧️"],
    [[71, 73, 75, 77, 85, 86], "🌨️"],
    [[95], "🌩️"],
    [[96, 99], "⛈️"],
  ]);
  const arr = [...icons.keys()].find((key) => key.includes(wmoCode));
  if (!arr) return "NOT FOUND";
  return icons.get(arr);
}

function formatDay(dateStr) {
  return new Intl.DateTimeFormat("en", {
    weekday: "short",
  }).format(new Date(dateStr));
}

export default function App() {
  const [location, setLocation] = useLocalStorageState("location", "Napoli");
  const { weather, isLoading, displayLocation } = useWeatherState(location);
  return (
    <div className="app">
      <h1>Weather App</h1>
      <Input location={location} onChangeLocation={setLocation} />

      {isLoading && <p className="loader">Loading...</p>}
      {weather.weathercode && (
        <Weather
          weather={weather}
          flag={displayLocation.flag}
          city={displayLocation.city}
        >
          <DayList weather={weather} />
        </Weather>
      )}
    </div>
  );
}

function Input({ location, onChangeLocation }) {
  return (
    <div>
      <input
        type="text"
        placeholder="Search location.."
        value={location}
        onChange={(e) => onChangeLocation(e.target.value)}
      />
    </div>
  );
}

function Weather({ children, flag, city }) {
  return (
    <div className="forecast">
      <h2>
        Weather {city}{" "}
        <img src={`https://flagcdn.com/24x18/${flag}.png`} alt="flag" />
      </h2>
      {children}
    </div>
  );
}

function DayList({ weather }) {
  const {
    temperature_2m_max: max,
    temperature_2m_min: min,
    time: dates,
    weathercode: codes,
  } = weather;

  return (
    <ul className="weather">
      {dates.map((date, i) => (
        <Day
          date={date}
          max={max.at(i)}
          min={min.at(i)}
          code={codes.at(i)}
          key={date}
          isToday={i === 0}
        />
      ))}
    </ul>
  );
}

function Day(props) {
  const { max, min, date, code, isToday } = props;
  return (
    <li className="day">
      <span>{getWeatherIcon(code)}</span>
      <p>{isToday ? "Today" : formatDay(date)}</p>
      <p>Max: {Math.ceil(max)}&deg;</p>
      <p>Min: {Math.floor(min)}&deg;</p>
    </li>
  );
}
