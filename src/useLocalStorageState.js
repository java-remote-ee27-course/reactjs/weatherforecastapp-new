import { useEffect, useState } from "react";

export function useLocalStorageState(key, initialState) {
  const [value, setValue] = useState(() => {
    const storedLocation = localStorage.getItem(key);
    return storedLocation ?? initialState;
  });

  //Load location from the localstorage
  useEffect(() => {
    localStorage.setItem(key, value);
  }, [key, value]);

  return [value, setValue];
}
